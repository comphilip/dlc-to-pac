package main

import (
	"flag"
	"fmt"
	"log"
	"strings"

	"gitlab.com/comphilip/dlc-to-pac/dlc"
	"gitlab.com/comphilip/dlc-to-pac/pac"
	"v2ray.com/core/app/router"
)

var (
	dlcFilePath        = flag.String("dlc-file", "", "dlc file path to parse")
	dlcUrl             = flag.String("dlc-url", "https://github.com/v2fly/domain-list-community/releases/latest/download/dlc.dat", "dlc url to download")
	directCountries    = flag.String("direct-country", "CN", "Sites whose country code not to proxy, seperated by comma")
	proxy              = flag.String("proxy", "PROXY 192.168.1.2:8080", "Proxy expression in pac file")
	extraDirectDomains = flag.String("extra-direct-domains", "", "Extra domains not proxy, seperated by comma")
)

func main() {
	flag.Parse()
	var sites *router.GeoSiteList
	var err error
	if *dlcFilePath != "" {
		sites, err = dlc.ParseDlcFromFile(*dlcFilePath)
		if err != nil {
			log.Panic(err)
		}
	} else if *dlcUrl != "" {
		sites, err = dlc.DownloadAndParseDlc(*dlcUrl)
		if err != nil {
			log.Panic(err)
		}
	} else {
		log.Panic("dlc-url is empty")
	}

	countryCodes := strings.FieldsFunc(*directCountries, func(c rune) bool { return c == ',' })
	for i, v := range countryCodes {
		countryCodes[i] = strings.TrimSpace(v)
	}

	extraDirectDomainList := strings.FieldsFunc(*extraDirectDomains, func(c rune) bool { return c == ',' })
	for i, v := range extraDirectDomainList {
		extraDirectDomainList[i] = strings.TrimSpace(v)
	}

	pacContent, err := pac.GeneratePAC(&pac.Options{
		Sites:              sites,
		DirectCountryCodes: countryCodes,
		Proxy:              *proxy,
		ExtraDirectDomains: extraDirectDomainList,
	})
	if err != nil {
		log.Panic(err)
	}
	fmt.Print(pacContent)
}
