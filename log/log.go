package log

import (
	"log"
	"os"
)

var (
	INFO  = log.New(os.Stderr, "[INFO] ", log.Lshortfile)
	WARN  = log.New(os.Stderr, "[WARN] ", log.Lshortfile)
	ERROR = log.New(os.Stderr, "[WARN] ", log.Lshortfile)
)
