# dlc to pac

Convert dlc.dat (can be download from https://github.com/v2ray/domain-list-community/raw/release/dlc.dat) into proxy.pac for browser auto proxy config.

This tool by default create proxy rules to `proxy` sites with country code `CN`. Use `-direct-country` to add more or custom no proxy contry codes.

Use `-proxy` for your proxy setting.

Use `-extra-direct-domains` to add custom no proxy domain.