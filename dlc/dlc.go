package dlc

import (
	"io/ioutil"
	"net/http"

	"github.com/golang/protobuf/proto"
	"gitlab.com/comphilip/dlc-to-pac/log"
	"v2ray.com/core/app/router"
)

func ParseDlcFromFile(dlcFilePath string) (*router.GeoSiteList, error) {
	data, err := ioutil.ReadFile(dlcFilePath)
	if err != nil {
		log.ERROR.Print(err)
		return nil, err
	}
	return parseDlc(data)
}

func DownloadAndParseDlc(dlcUrl string) (*router.GeoSiteList, error) {
	resp, err := http.DefaultClient.Get(dlcUrl)
	if err != nil {
		log.ERROR.Print(err)
		return nil, err
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.ERROR.Print(err)
		return nil, err
	}
	return parseDlc(data)
}

func parseDlc(data []byte) (*router.GeoSiteList, error) {
	var list router.GeoSiteList
	if err := proto.Unmarshal(data, &list); err != nil {
		log.ERROR.Print(err)
		return nil, err
	}
	return &list, nil
}
