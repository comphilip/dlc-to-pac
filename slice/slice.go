package slice

import (
	"log"
	"reflect"
)

func Index(slice, v interface{}) int {
	fv := reflect.ValueOf(slice)
	if fv.Kind() != reflect.Slice {
		log.Panic("slice arg must be type of slice")
	}
	for i := 0; i < fv.Len(); i++ {
		if reflect.DeepEqual(v, fv.Index(i).Interface()) {
			return i
		}
	}
	return -1
}

func Exists(slice, v interface{}) bool {
	return Index(slice, v) != -1
}

func IndexFunc(slice interface{}, f func(i int) bool) int {
	fv := reflect.ValueOf(slice)
	if fv.Kind() != reflect.Slice {
		log.Panic("slice arg must be type of slice")
	}
	for i := 0; i < fv.Len(); i++ {
		if f(i) {
			return i
		}
	}
	return -1
}

func ExistsFunc(slice interface{}, f func(i int) bool) bool {
	return IndexFunc(slice, f) != -1
}
