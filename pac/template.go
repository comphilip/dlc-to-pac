package pac

const FIND_PROXY_FOR_URL = `
function FindProxyForURL(url, host) {
  if (ipregex.test(host)) {
    return noproxy;
  }
  while (host != "") {
    if (noProxyDomains[host] === 1) {
      return noproxy;
    }
    index = host.indexOf('.');
    host = index != -1 ? host.substring(index+1) : "";
  }
  return proxy;
}
`
