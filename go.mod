module gitlab.com/comphilip/dlc-to-pac

go 1.14

require (
	github.com/golang/protobuf v1.4.2
	v2ray.com/core v4.19.1+incompatible
)
