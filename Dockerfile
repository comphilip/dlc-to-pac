FROM golang:alpine as builder

WORKDIR /usr/src/app
COPY . .
RUN go build -v -o dlc-to-pac

FROM alpine
RUN apk --no-cache add ca-certificates
COPY --from=builder /usr/src/app/dlc-to-pac /usr/local/bin/dlc-to-pac
ENTRYPOINT ["/usr/local/bin/dlc-to-pac"]
