package pac

import (
	"fmt"
	"strings"

	"gitlab.com/comphilip/dlc-to-pac/slice"
	"v2ray.com/core/app/router"
)

type Options struct {
	Sites              *router.GeoSiteList
	DirectCountryCodes []string
	Proxy              string
	ExtraDirectDomains []string
}

func GeneratePAC(op *Options) (string, error) {
	noProxyDomains := make(map[string]struct{}, 1024*5)
	for _, v := range op.ExtraDirectDomains {
		if _, found := noProxyDomains[v]; !found {
			noProxyDomains[v] = struct{}{}
		}
	}
	for _, site := range op.Sites.Entry {
		if !slice.Exists(op.DirectCountryCodes, site.CountryCode) {
			continue
		}
		for _, domain := range site.Domain {
			if domain.Type == router.Domain_Regex {
				// not support regex
				continue
			}
			if _, found := noProxyDomains[domain.Value]; !found {
				noProxyDomains[domain.Value] = struct{}{}
			}
		}
	}
	// bulid pac file
	builder := &strings.Builder{}
	builder.WriteString(fmt.Sprintf(`var proxy = "%s";`, op.Proxy))
	builder.WriteString("\n")
	builder.WriteString(`var noproxy = "DIRECT";`)
	builder.WriteString("\n")
	builder.WriteString(`var ipregex = /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/;`)
	builder.WriteString("\n")
	builder.WriteString("var noProxyDomains = {")
	domains := make([]string, 0, len(noProxyDomains))
	for name := range noProxyDomains {
		domains = append(domains, fmt.Sprintf(`"%s":1`, name))
	}
	builder.WriteString(strings.Join(domains, ","))
	builder.WriteString("};\n")
	builder.WriteString(FIND_PROXY_FOR_URL)
	return builder.String(), nil
}
